﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopShare
{
    /// <summary>
    /// 0类型通道：1图片 2命令 3音频
    /// 1数据通道：
    ///           [1图片]:0x0A图像头 0x0B图像Body 0x0C图像尾
    ///           [2命令]:0x0E 状态命令
    ///           [3音频]:0x0F
    /// </summary>
    public class RTP
    {
        public static byte[] SetCommond(byte[] arr)
        {
            byte[] res = new byte[arr.Length + 2];
            arr.CopyTo(res, 2);
            res[0] = 2;
            res[1] = 0x0E;
            return res;
        }

        public static byte[] SetFirst(byte[] arr)
        {
            byte[] res = new byte[arr.Length + 2];
            arr.CopyTo(res, 2);
            res[0] = 1;
            res[1] = 0x0A;
            return res;
        }

        public static byte[] SetMiddle(byte[] arr)
        {
            byte[] res = new byte[arr.Length + 2];
            arr.CopyTo(res, 2);
            res[0] = 1;
            res[1] = 0x0B;
            return res;
        }

        public static byte[] SetLast(byte[] arr)
        {
            byte[] res = new byte[arr.Length + 2];
            arr.CopyTo(res, 2);
            res[0] = 1;
            res[1] = 0x0C;
            return res;
        }

        public static string GetCommond(byte[] data)
        {
            byte[] des = new byte[data.Length - 2];
            Array.Copy(data, 2, des, 0, des.Length);
            return Encoding.UTF8.GetString(des).Trim();
        }

        public static byte[] GetFullData(List<byte[]> bys)
        {
            //int count = 0;
            List<byte> res = new List<byte>();
            for (int i = 0; i < bys.Count; i++)
            {
                //count += (bys[i].Length - 1);
                for (int j = 2; j < bys[i].Length; j++)
                {
                    res.Add(bys[i][j]);
                }
            }
            return res.ToArray();
        }

        public static void GetFullData(List<byte[]> bys, MemoryStream ms)
        {
            //int count = 0;
            List<byte> res = new List<byte>();
            for (int i = 0; i < bys.Count; i++)
            {
                //count += (bys[i].Length - 1);
                for (int j = 2; j < bys[i].Length; j++)
                {
                    res.Add(bys[i][j]);
                }
            }
            ms.Write(res.ToArray(), 0, res.Count);
        }
    }
}
