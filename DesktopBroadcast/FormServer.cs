﻿using DesktopShare;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopBroadcast
{
    public partial class FormServer : Form
    {
        Socket ServerSocket;
        ConfigItem SerConfig;
        IPEndPoint GroupEP;

        System.Windows.Forms.Timer ScreenTimer;

        public FormServer()
        {
            InitializeComponent();
            SerConfig = ConfigItem.GetConfig();
        }

        private void tsStart_Click(object sender, EventArgs e)
        {
            ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            GroupEP = new IPEndPoint(IPAddress.Parse(SerConfig.BroadcastAddress), SerConfig.BroadcastPort);

            ServerSocket.SendBufferSize = 1024 * 1024 * 5;

            if (ScreenTimer == null)
            {
                ScreenTimer = new System.Windows.Forms.Timer();
                ScreenTimer.Interval = SerConfig.Interval;
                ScreenTimer.Tick += ScreenTimer_Tick;
            }
            ScreenTimer.Enabled = true;
            AppendLog(string.Empty, string.Format("服务已启动...({0}:{1})", SerConfig.BroadcastAddress, SerConfig.BroadcastPort));
            tsStart.Enabled = false;
            tsStop.Enabled = true;
        }

        private void AppendLog(string tag, string info)
        {
            try
            {
                if (string.IsNullOrEmpty(tag))
                    tbInfo.AppendText(info + Environment.NewLine);
                else
                    tbInfo.AppendText(tag + ": " + info + Environment.NewLine);
            }
            catch
            {

            }
        }

        private void ScreenTimer_Tick(object sender, EventArgs e)
        {
            var screen = Desktop.ImageToByte(Desktop.GetScreen());
            int size = 0;
            int count = 1399;
            while (size < screen.Length)
            {
                byte[] temp = null;
                if (size == 0)
                {
                    temp = RTP.SetFirst(screen.Skip(size).Take(count).ToArray());
                }
                else if (size + count > screen.Length)
                {
                    temp = RTP.SetLast(screen.Skip(size).Take(count).ToArray());
                }
                else
                {
                    temp = RTP.SetMiddle(screen.Skip(size).Take(count).ToArray());
                }
                ServerSocket.SendTo(temp, GroupEP);
                size += count;
            }
        }

        private void tsStop_Click(object sender, EventArgs e)
        {
            ScreenTimer.Enabled = false;

            ServerSocket.Close();

            tsStart.Enabled = true;
            tsStop.Enabled = false;
            tsSystemConfig.Enabled = true;
            AppendLog(string.Empty, "服务已关闭...");
        }

        private void tsSystemConfig_Click(object sender, EventArgs e)
        {
            FormConfig config = new FormConfig();
            config.ShowDialog();
        }

        private void tsExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tsShowMain_Click(object sender, EventArgs e)
        {
            this.Show();
        }
    }


}
