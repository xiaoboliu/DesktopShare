﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopBroadcast
{
    public partial class FormConfig : Form
    {
        public FormConfig()
        {
            InitializeComponent();
            Load += ShareConfig_Load;
            FormClosing += ShareConfig_FormClosing;
        }

        private void ShareConfig_Load(object sender, EventArgs e)
        {
            ConfigItem data = ConfigItem.GetConfig();
            propertySharing.SelectedObject = data;
        }

        private void ShareConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            ConfigItem.Persistent((ConfigItem)propertySharing.SelectedObject);
        }
    }
}
