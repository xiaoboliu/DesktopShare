﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DesktopClient
{
    public class ConfigItem
    {
        public ConfigItem()
        {
            BroadcastAddress = "234.0.0.1";
            BroadcastPort = 7788;
        }

        [Category("数据"), Description("广播地址")]
        public string BroadcastAddress { get; set; }

        [Category("数据"), Description("端口")]
        public int BroadcastPort { get; set; }


        public static void Persistent(List<ConfigItem> data)
        {
            using (FileStream fs = new FileStream("ClientConfig.xml", FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<ConfigItem>));
                xs.Serialize(fs, data);
            }
        }

        public static List<ConfigItem> GetConfig()
        {
            List<ConfigItem> config = new List<ConfigItem>();
            if (!File.Exists("ClientConfig.xml"))
                return config;
            using (FileStream fs = new FileStream("ClientConfig.xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                try
                {
                    XmlSerializer xs = new XmlSerializer(typeof(List<ConfigItem>));
                    var temp = xs.Deserialize(fs) as List<ConfigItem>;
                    if (temp != null)
                        config = temp;
                }
                catch
                {
                    return config;
                }
            }
            return config;
        }
    }

    public class BroadcastItem
    {
        public BroadcastItem()
        {

        }
        public BroadcastItem(string key, int port)
        {
            this.Key = key;
            this.BroadcastAddress = IPAddress.Parse(key);
            this.BroadcastPort = port;
        }

        public string Key { get; set; }
        public IPAddress BroadcastAddress { get; set; }
        public int BroadcastPort { get; set; }
        public DesktopShare.BroadcastSocket Socket { get; set; }

    }
}
