﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopClient
{
    public partial class FormAdd : Form
    {
        List<BroadcastItem> Items;
        Action a;
        public FormAdd(List<BroadcastItem> lst, Action a)
        {
            InitializeComponent();
            this.Items = lst;
            this.a = a;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Items.Add(new BroadcastItem(tbAddress.Text.Trim(), int.Parse(tbPort.Text.Trim())));
                a.Invoke();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
